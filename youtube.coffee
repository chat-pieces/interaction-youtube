{ promises:fs, createWriteStream:fileWriteStream } = require 'fs'
{ timeout, interval } = require 'small-simple-bot/helpers/timing'
run = require '@aurium/run'

mkID = -> Math.random().toString(36).split(".")[1]

memoFile = './videos.memo'
queue = []
mb = 1024

ytER = ///
https?://(?:www\.)?
(?:youtube\.com/watch\?[-_a-z0-9&=]*\bv=|youtu.be/)
([-_a-z0-9]+)
///i

getUrlER = ///
https?://
[-_a-z0-9]+(\.[-_a-z0-9]+)+
(/[-_a-z0-9\./]*)?
(\?[-_a-z0-9\.=&]+)?
///i


module.exports = youtube = (bot, update)->
    msg = update.message?.text or ''
    usr = update._act.from.username
    if update.toMe and msg
        youtubeId = msg.match(ytER)?[1]
        if youtubeId or msg.match /\/video_dl\b/
            # The user send a valid youtube video url, or explicit ask for download.
            url = if youtubeId then 'http://youtu.be/' + youtubeId else msg.match(getUrlER)?[0]
            chatId = update._act.chat.id
            msgId = update.message.message_id
            msgConf = reply_to_message_id: msgId
            unless url
                return bot.sendMessage "I don't see an url.", chatId, msgConf
            queryMemo url
            .then (info)->
                if info
                    resp = "TODO: send video.\n#{JSON.stringify info}"
                    bot.sendMessage resp, chatId, msgConf
                else
                    if queue.length < 10 or bot.isAdm usr
                        queue.push [update, url]
                    else
                        bot.sendMessage 'The download queue is too big.', chatId, msgConf
            .catch (err)->
                bot.sendMessage "Ups... I can't download now.", chatId, msgConf
                bot.logErrorAndMsgAdm err, 'Trying to query video memo.'


queryMemo = (url)->
    fs.readFile memoFile, 'utf8'
    .then (lines)->
        line = lines.split('\n').find (line)-> line.match ///^#{url} ///i
        if line
            JSON.parse "{#{ line.replace /^[^ ]+/, '' }}"
        else
            null
    .catch (err)->
        if err?.code is 'ENOENT'
            null
        else
            throw err


module.exports.init = (bot)->
    processQueue bot


processQueue = (bot)->
    return timeout(20, -> processQueue bot) if queue.length is 0
    [update, url] = queue.shift()
    bot.logger.log "Downloading video #{url}..."
    chatId = update._act.chat.id
    msgId = update.message.message_id
    msgConf = reply_to_message_id: msgId
    run 'youtube-dl', '--dump-json', url
    .then (info)->
        info = JSON.parse info
        downloadVideo bot, url, info, chatId, msgId, msgConf
    .catch (err)->
        bot.sendMessage "Fail to download. #{err.message}", chatId, msgConf
        bot.logErrorAndMsgAdm err, 'Fail to get video info.'
    .finally ->
        processQueue bot


downloadVideo = (bot, url, info, chatId, msgId, msgConf)->
    videoFileBase = "/tmp/bot-#{bot.username}-video-download-#{mkID()}"
    videoFile = videoFileBase + '.mp4'
    run 'youtube-dl', '--format=bestvideo[tbr<500]+bestaudio[abr<130]', '--recode-video', 'mp4', '--postprocessor-args', '-b:v 200k -b:a 90k', '-o', videoFileBase, url
    .then ->
        videoMsgConf = { caption: info.title, msgConf... }
        bot.sendVideo videoFile, chatId, videoMsgConf, (err, resp, result)->
            if err
                bot.sendMessage "Fail to send video. #{err.message}", chatId, msgConf
                bot.logErrorAndMsgAdm err, 'Fail to send video to user.'
            else
                fs.appendFile memoFile, "#{url} fileId:#{result.video?.file_id}"
                .catch (err)->
                    bot.logErrorAndMsgAdm err, "Can't update #{memoFile}"
            processingVideoSince = false



notifyDownloadProgress = (bot, chatId, msgConf, stream, size)->
    totStreamed = 0
    lastTotStreamed = 0
    stream.on 'data', (chunk)-> totStreamed += chunk.length
    bot.sendMessage "Downloading...", chatId, msgConf, (err, resp, result)->
        return bot.logErrorAndMsgAdm err, 'Cant notify download.' if err
        msgId = result.message_id
        do showProgress = ->
            pct = Math.round(10000 * totStreamed / size) / 100
            unless totStreamed is lastTotStreamed
                bot.editMessageText "
                Download progress: #{totStreamed} of #{size} (#{pct}%)
                ", chatId, msgId
            lastTotStreamed = totStreamed
            if totStreamed < size
                timeout 5, showProgress
            else
                bot.deleteMessage chatId, msgId

